from flask import Flask, jsonify, request
from config.db_config import DB_ACCESS


def create_app():
    app = Flask(__name__)

    @app.route("/ping", methods=["GET"])
    def ping():
        return jsonify("Pong")

    @app.route("/log", methods=["POST"])
    def log():
        try:
            user_id, message, message_time, extra_params = parse_message_from_request(request.get_json(force=True))
            DB_ACCESS().save_chat_log_to_database(user_id, message, message_time, extra_params)
            return jsonify("Log Saved!")
        except Exception as e:
            return jsonify(e)

    return app


def parse_message_from_request(request_json):
    user_id = request_json['user_id']
    message = request_json['message']
    message_time = request_json['message_time']
    extra_params = {key: request_json[key] for key in request_json.keys() if
                    key not in ['user_id', 'message', 'message_time']}
    return user_id, message, message_time, extra_params
