class IDatabaseAccess(object):
    def __init__(self):
        self.connection = None
        self.initialize_database_connection()

    def initialize_database_connection(self):
        """
        Initializes the connection to the database (This part should be the only time we initialize the connection if
        it is a persistent one.

        Basically this should be called minimal times, and should guerentee the other functions will work towards the db
        """
        pass

    def validate_database_container(self):
        """
        Validate that all the containers inside the database (Tables in Oracle, Index in elastic and so on) are valid,
        so each time we try to save there will be a container to save in to.
        """
        pass

    def save_chat_log_to_database(self, user_identifier, message, message_timestamp, extra_parameters):
        """
        Saves a chat log received from one of the "listening" clients to the database.

        :param user_identifier: The user identifier (Could be a name, or a UUID, depends on the chatting service)
        :type user_identifier: str
        :param message: The message that was sent in the chat.
        :type message: str
        :param message_timestamp: The time the message was sent.
        :type message_timestamp: str or datetime
        :param extra_parameters: A dictionary of extra parameters to save in case of need.
        :type extra_parameters: dict
        """
        pass
