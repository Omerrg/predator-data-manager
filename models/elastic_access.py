from models.database_access import IDatabaseAccess
from elasticsearch import Elasticsearch
from config.config import SYSTEM_ENVIRONMENT

ELASTIC_HOSTS = [{'host': 'localhost', 'port': 9200}]
ELASTIC_INDEX_NAME = "predator_chat_logs-{env}".format(env=SYSTEM_ENVIRONMENT)


class ElasticAccess(IDatabaseAccess):
    def initialize_database_connection(self):
        self.connection = Elasticsearch(hosts=ELASTIC_HOSTS)

    def validate_database_container(self):
        if not self.connection.indices.exists(ELASTIC_INDEX_NAME):
            self.connection.indices.create(ELASTIC_INDEX_NAME)

    def save_chat_log_to_database(self, user_identifier, message, message_timestamp, extra_parameters):
        self.validate_database_container()
        body = {"user_identifier": user_identifier,
                "message": message,
                "message_timestamp": message_timestamp}
        for parameter_type in extra_parameters.keys():
            body[parameter_type] = extra_parameters[parameter_type]

        self.connection.index(index=ELASTIC_INDEX_NAME, body=body)
